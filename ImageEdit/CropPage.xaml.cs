﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;
using Lumia.Imaging;
using Lumia.Imaging.Transforms;

// Документацию по шаблону элемента "Пустая страница" см. по адресу https://go.microsoft.com/fwlink/?LinkId=234238

namespace ImageEdit
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class CropPage : Page
    {
        private Point startPoint;
        private Point endPoint;

        private SwapChainPanelRenderer m_renderer;

        public CropPage()
        {
            this.InitializeComponent();

            SwapChainPanelTarget.Loaded += SwapChainPanelTarget_Loaded;
        }

        private void Grid_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            if ((translateTransform.X + Rect.Width) < SwapChainPanelTarget.RenderSize.Width)        
            {
                translateTransform.X += e.Delta.Translation.X;
                rectTranslateTransform.X += e.Delta.Translation.X;
            }
            else
            {
                translateTransform.X -= 1;
                rectTranslateTransform.X -= 1;
            }
            if((translateTransform.Y + Rect.Height) < SwapChainPanelTarget.RenderSize.Height)
            {
                translateTransform.Y += e.Delta.Translation.Y;
                rectTranslateTransform.Y += e.Delta.Translation.Y;
            }
            else
            {
                translateTransform.Y -= 1;
                rectTranslateTransform.Y -= 1;
            }
            Debug.WriteLine(translateTransform.Y);
            e.Handled = true;
        }


        private async void SwapChainPanelTarget_Loaded(object sender, RoutedEventArgs e)
        {

            m_renderer = new SwapChainPanelRenderer(Data.Provider, SwapChainPanelTarget);

            await m_renderer.RenderAsync();

            

            SwapChainPanelTarget.SizeChanged += async (s, args) =>
            {
                if(m_renderer != null)
                    await m_renderer.RenderAsync();
                
            };
        }

        private async Task cropImage(Point startPoint, Point endPoint)
        {
            await ApplyEffectAsync(new CropEffect(Data.Provider, new Rect(startPoint, endPoint)));
        }

        private async Task<bool> ApplyEffectAsync(IImageProvider provider)
        {

            string errorMessage = null;

            try
            {

                // Set the imageSource on the effect and render.
                m_renderer = new SwapChainPanelRenderer(provider, SwapChainPanelTarget);

                await m_renderer.RenderAsync();
                

            }
            catch (Exception exception)
            {
                errorMessage = exception.Message;
            }

            if (!string.IsNullOrEmpty(errorMessage))
            {
                var dialog = new MessageDialog(errorMessage);
                await dialog.ShowAsync();
                return false;
            }

            return true;
        }


        private void DoneButton_Click(object sender, RoutedEventArgs e)
        {
            
            var temp = new CropEffect(Data.Provider,
                new Rect(translateTransform.X, translateTransform.Y, CropArea.Width,
                    CropArea.Height));


            Data.Provider = temp.Clone();
            Data.AddToStateList(Data.Provider);
            Frame.Navigate(typeof(MainPage));

        }


        private void Rect_OnManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            Rect.Width += e.Delta.Translation.X;
            Rect.Height += e.Delta.Translation.Y;

            CropArea.Width += e.Delta.Translation.X;
            CropArea.Height += e.Delta.Translation.Y;
        }
    }
}
