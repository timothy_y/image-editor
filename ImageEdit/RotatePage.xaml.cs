﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;
using Lumia.Imaging;
using Lumia.Imaging.Transforms;

// Документацию по шаблону элемента "Пустая страница" см. по адресу https://go.microsoft.com/fwlink/?LinkId=234238

namespace ImageEdit
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class RotatePage : Page
    {
        private SwapChainPanelRenderer m_renderer;

        public RotatePage()
        {
            this.InitializeComponent();

            SwapChainPanelTarget.Loaded += SwapChainPanelTarget_Loaded;

        }

        private async void SwapChainPanelTarget_Loaded(object sender, RoutedEventArgs e)
        {
            m_renderer = new SwapChainPanelRenderer(Data.Provider, SwapChainPanelTarget);

            await m_renderer.RenderAsync();

            SwapChainPanelTarget.SizeChanged += async (s, args) =>
            {
                await m_renderer.RenderAsync();
            };
        }


        private async void rotateButton_Click(object sender, RoutedEventArgs e)
        {
            var temp = new RotationEffect(Data.Provider, 90);
            Data.Provider = temp.Clone();
            await ApplyEffectAsync(Data.Provider);
        }

        private async void vFlipButton_Click(object sender, RoutedEventArgs e)
        {
            var temp = new FlipEffect(Data.Provider, FlipMode.Horizontal);
            Data.Provider = temp.Clone();
            await ApplyEffectAsync(Data.Provider);

        }

        private async Task<bool> ApplyEffectAsync(IImageProvider provider)
        {

            string errorMessage = null;

            try
            {

                // Set the imageSource on the effect and render.
                m_renderer = new SwapChainPanelRenderer(provider, SwapChainPanelTarget);



                await m_renderer.RenderAsync();

            }
            catch (Exception exception)
            {
                errorMessage = exception.Message;
            }

            if (!string.IsNullOrEmpty(errorMessage))
            {
                var dialog = new MessageDialog(errorMessage);
                await dialog.ShowAsync();
                return false;
            }

            return true;
        }

        private void DoneButton_Click(object sender, RoutedEventArgs e)
        {
            Data.AddToStateList(Data.Provider);
            Frame.Navigate(typeof(MainPage));
        }

        private async void hFlipButton_Click(object sender, RoutedEventArgs e)
        {
            var temp = new FlipEffect(Data.Provider, FlipMode.Vertical);
            Data.Provider = temp.Clone();
            await ApplyEffectAsync(Data.Provider);
        }
    }
}
