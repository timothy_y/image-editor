﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Lumia.Imaging;

namespace ImageEdit
{
    internal static class Data
    {
        public static MainPage MainPage;

        public static IImageProvider Provider;

        public static IImageProvider OriginalProvider;

        public static List<IImageProvider> ProviderList = new List<IImageProvider>();

        public static int CurrentStateIndex = 0;

        public static void AddToStateList(IImageProvider provider)
        {
            MainPage.undoButtonRef.IsEnabled = true;
            ProviderList.Add(provider);
            CurrentStateIndex++;
        }

        public static IImageProvider Redo()
        {
            if (CurrentStateIndex == ProviderList.Count)
            {
                MainPage.redoButtonRef.IsEnabled = false;
                MainPage.undoButtonRef.IsEnabled = true;
                return ProviderList[CurrentStateIndex - 1];
            }
            else
            {
                CurrentStateIndex++;
                MainPage.redoButtonRef.IsEnabled = true;
                MainPage.undoButtonRef.IsEnabled = true;
                return ProviderList[CurrentStateIndex - 1];
            }
        }

        public static IImageProvider Undo()
        {
            if (CurrentStateIndex == 1)
            {
                MainPage.undoButtonRef.IsEnabled = false;
                MainPage.redoButtonRef.IsEnabled = true;
                return ProviderList[CurrentStateIndex - 1];
            }
            else
            {
                CurrentStateIndex--;
                MainPage.undoButtonRef.IsEnabled = true;
                MainPage.redoButtonRef.IsEnabled = true;
                return ProviderList[CurrentStateIndex];
            }
        }


    }
}
