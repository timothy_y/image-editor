﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.Graphics.Imaging;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Automation.Peers;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Media.Imaging;
using Lumia.Imaging;
using Lumia.Imaging.Adjustments;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Lumia.Imaging.Artistic;

// Документацию по шаблону элемента "Пустая страница" см. по адресу https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x419

namespace ImageEdit
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private SwapChainPanelRenderer m_renderer;

        private IImageProvider imageProvider;

        private Windows.Storage.StorageFile imageFile;

        public AppBarButton undoButtonRef;
        public AppBarButton redoButtonRef;

        private Dictionary<string, IImageProvider> filterDictionary;

        public MainPage()
        {
            this.InitializeComponent();

            Data.MainPage = this;
            undoButtonRef = undoButton;
            redoButtonRef = redoButton;        

            if (Data.CurrentStateIndex != 0)
            {
                undoButton.IsEnabled = true;
            }
            
            SwapChainPanelTarget.Loaded += SwapChainPanelTarget_Loaded;

        }

        private async void SwapChainPanelTarget_Loaded(object sender, RoutedEventArgs e)
        {

            if (Data.Provider != null)
            {
                imageProvider = Data.Provider;
                await ApplyEffectAsync(Data.Provider);

                setFilters(imageProvider);
            }
                

            SwapChainPanelTarget.SizeChanged += async (s, args) =>
            {
                if(m_renderer != null)
                    await m_renderer.RenderAsync();
            };
        }


        private async void openButton_Click(object sender, RoutedEventArgs e)
        {
            var picker = new Windows.Storage.Pickers.FileOpenPicker()
            {
                ViewMode = Windows.Storage.Pickers.PickerViewMode.Thumbnail,
                SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.PicturesLibrary
            };

            picker.FileTypeFilter.Add(".jpg");
            picker.FileTypeFilter.Add(".jpeg");
            picker.FileTypeFilter.Add(".png");
            
            imageFile = await picker.PickSingleFileAsync();

            if (imageFile != null)
            {
                var fileStream = await imageFile.OpenAsync(Windows.Storage.FileAccessMode.Read);
 

                imageProvider = new RandomAccessStreamImageSource(fileStream);

                Data.OriginalProvider = imageProvider;
                Data.ProviderList.Add(imageProvider);
                

                m_renderer = new SwapChainPanelRenderer(imageProvider, SwapChainPanelTarget);

            
                await m_renderer.RenderAsync();


                setFilters(Data.OriginalProvider);
            }
            else
            {

            }
        }

        private async void setFilters(IImageProvider source)
        {
            filterDictionary = new Dictionary<string,IImageProvider>();

            filterDictionary.Add("Sepia", new SepiaEffect(source));
            filterDictionary.Add("ColorBoost", new ColorBoostEffect(source));
            filterDictionary.Add("Grayscale", new GrayscaleEffect(source));
            filterDictionary.Add("Antique", new AntiqueEffect(source));
            filterDictionary.Add("LomoEffect", new LomoEffect(source));
            filterDictionary.Add("Negative", new NegativeEffect(source));


            foreach (var filter in filterDictionary)
            {
                var bitmap = new BitmapImage();
                using (var jpegRenderer = new JpegRenderer(filter.Value))
                {
                    IBuffer jpegBuffer = await jpegRenderer.RenderAsync();
                    await bitmap.SetSourceAsync(jpegBuffer.AsStream().AsRandomAccessStream());

                    ((Button) FindName(filter.Key)).Content = new Image()
                    {
                        Source = bitmap,
                        Height = 120,
                        Width = 120,
                        Stretch = Stretch.UniformToFill,
                        Margin = new Thickness(-10, -7, -10, -7)
                    };

                    ((Button)FindName(filter.Key)).Click += filterButton_click;
                }        
            }
        }

        private async void filterButton_click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;

            var filterName = button.Name;

            var temp = filterDictionary[filterName];

            imageProvider = temp;

            Data.AddToStateList(imageProvider);

            await this.ApplyEffectAsync(filterDictionary[filterName]);

        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            var savePicker = new Windows.Storage.Pickers.FileSavePicker()
            {
                SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.PicturesLibrary,
                SuggestedFileName = string.Format("CartoonImage_{0}", DateTime.Now.ToString("yyyyMMddHHmmss"))
            };

            savePicker.FileTypeChoices.Add("JPG File", new List<string> { ".jpg" });
            SaveImage(savePicker);
            
        }

        private async void SaveImage(Windows.Storage.Pickers.FileSavePicker savePicker)
        {
            var file = await savePicker.PickSaveFileAsync();
            if (file != null)
            {
                await SaveImageAsync(file);
            }
        }

        private async Task<bool> ApplyEffectAsync(IImageProvider provider)
        {
           
            string errorMessage = null;

            try
            {

                // Set the imageSource on the effect and render.
                m_renderer = new SwapChainPanelRenderer(provider, SwapChainPanelTarget);

                

                await m_renderer.RenderAsync();

            }
            catch (Exception exception)
            {
                errorMessage = exception.Message;
            }

            if (!string.IsNullOrEmpty(errorMessage))
            {
                var dialog = new MessageDialog(errorMessage);
                await dialog.ShowAsync();
                return false;
            }

            return true;
        }

        private async Task<bool> SaveImageAsync(StorageFile file)
        {

            string errorMessage = null;

            try
            {
                using (var jpegRenderer = new JpegRenderer(imageProvider))
                using (var stream = await file.OpenAsync(FileAccessMode.ReadWrite))
                {
                    // Jpeg renderer gives the raw buffer that contains the filtered image.
                    IBuffer jpegBuffer = await jpegRenderer.RenderAsync();
                    await stream.WriteAsync(jpegBuffer);
                    await stream.FlushAsync();
                }
            }
            catch (Exception exception)
            {
                errorMessage = exception.Message;
            }

            if (!string.IsNullOrEmpty(errorMessage))
            {
                var dialog = new MessageDialog(errorMessage);
                await dialog.ShowAsync();
                return false;
            }
            return true;
        }

        private async void brightnessSlider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            

            var brightnessEffect = new BrightnessEffect(imageProvider);
           

            brightnessEffect.Level = e.NewValue;         


            await this.ApplyEffectAsync(brightnessEffect);
        }

        private async void contrastSlider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            var contrastEffect = new ContrastEffect(imageProvider);

            contrastEffect.Level = e.NewValue;

            await this.ApplyEffectAsync(contrastEffect);
        }

        private async void hdrSlider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            var hdrEffect = new SharpnessEffect(imageProvider);

            hdrEffect.Level = e.NewValue;

            await this.ApplyEffectAsync(hdrEffect);
        }

        private async void vibranceSlider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            var vibranceEffect = new VibranceEffect(imageProvider);

            vibranceEffect.Level = e.NewValue;

            await this.ApplyEffectAsync(vibranceEffect);
        }

        private void rotateButton_Click(object sender, RoutedEventArgs e)
        {
            Data.Provider = imageProvider;
            Frame.Navigate(typeof(RotatePage));        
        }

        private void cropButton_Click(object sender, RoutedEventArgs e)
        {
            Data.Provider = imageProvider;
            Frame.Navigate(typeof(CropPage));
        }

        private async void redoButton_Click(object sender, RoutedEventArgs e)
        {
            imageProvider = Data.Redo();
            await this.ApplyEffectAsync(imageProvider);
        }

        private async void undoButton_Click(object sender, RoutedEventArgs e)
        {
            imageProvider = Data.Undo();
            await this.ApplyEffectAsync(imageProvider);
        }

        private void BrightnessSlider_OnPointerReleased(object sender, PointerRoutedEventArgs e)
        {
            var brightnessEffect = new BrightnessEffect(imageProvider);
            brightnessEffect.Level = brightnessSlider.Value;

            imageProvider = brightnessEffect.Clone();

            Data.AddToStateList(imageProvider);
        }

        private void ContrastSlider_OnPointerReleased(object sender, PointerRoutedEventArgs e)
        {
            var contrastEffect = new ContrastEffect(imageProvider);

            contrastEffect.Level = contrastSlider.Value;

            Data.AddToStateList(imageProvider);
        }

        private void HdrSlider_OnPointerReleased(object sender, PointerRoutedEventArgs e)
        {
            var hdrEffect = new SharpnessEffect(imageProvider);

            hdrEffect.Level = hdrSlider.Value;

            Data.AddToStateList(imageProvider);
           
        }

        private void VibranceSlider_OnPointerReleased(object sender, PointerRoutedEventArgs e)
        {
            var vibranceEffect = new VibranceEffect(imageProvider);

            vibranceEffect.Level = vibranceSlider.Value;

            Data.AddToStateList(imageProvider);
        }
    }
}
